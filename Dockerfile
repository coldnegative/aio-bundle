FROM alpine:3.9


# System-wide

RUN \
  apk add -U --no-progress --no-cache openssh-client xorriso git jq curl unzip qemu-img make


# Ansible + mitogen

RUN \
  apk add -U --no-progress --no-cache libbz2 expat libffi gdbm ncurses-terminfo-base ncurses-terminfo ncurses-libs readline sqlite-libs python2 py-pip gcc python-dev musl-dev libffi-dev openssl-dev && \
  pip install --no-cache-dir ansible==2.9.6 netaddr==0.7.19 pbr==5.4.4 hvac==0.8.2 ruamel.yaml==0.16.10 jmespath==0.9.5 && \
  find /usr/lib/python2.7 -name \*.pyc | xargs rm && \
  curl -sL https://files.pythonhosted.org/packages/source/m/mitogen/mitogen-0.2.8.tar.gz | tar xzvf - -C /opt/ && \
  mkdir -p /etc/ansible && \
  echo -e '[defaults]\nhost_key_checking=False\ndeprecation_warnings=False\n[connection]\npipelining=True\n[ssh_connection]\npipelining=True\n' > /etc/ansible/ansible.cfg


# Terraform

RUN \
  wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip && \
  unzip terraform_0.11.13_linux_amd64.zip && \
  rm terraform_0.11.13_linux_amd64.zip && \
  mv terraform /bin && \
  mkdir -p /root/.terraform.d/plugins


# Terraform plugins

RUN \
  apk add -U --no-progress --no-cache libvirt-client && \
  wget -O terraform-provider-libvirt "https://gitlab.com/egeneralov/terraform-provider-libvirt/-/jobs/artifacts/fix-1/raw/terraform-provider-libvirt?job=alpine" && \
  chmod +x terraform-provider-libvirt && \
  mv terraform-provider-libvirt /root/.terraform.d/plugins

RUN \
  mkdir -p /root/.terraform.d/plugins && \
  wget -O terraform-provisioner-ansible "https://github.com/radekg/terraform-provisioner-ansible/releases/download/v2.1.2/terraform-provisioner-ansible-linux-amd64_v2.1.2" && \
  chmod +x terraform-provisioner-ansible && \
  mv terraform-provisioner-ansible /root/.terraform.d/plugins

RUN \
  wget https://github.com/adammck/terraform-inventory/releases/download/v0.8/terraform-inventory_v0.8_linux_amd64.zip && \
  unzip terraform-inventory_v0.8_linux_amd64.zip && \
  rm terraform-inventory_v0.8_linux_amd64.zip && \
  mv terraform-inventory /bin/

# Docker

RUN \
  curl -LO https://download.docker.com/linux/static/stable/x86_64/docker-19.03.1.tgz && \
  tar xvf docker-19.03.1.tgz && \
  mv docker/docker /bin/docker && \
  rm -rf docker-19.03.1.tgz docker




# docker-compose from wernight/docker-compose
ENV LD_LIBRARY_PATH=/lib:/usr/lib

RUN \
  set -x && \
  apk add --no-cache -t .deps ca-certificates && \
  wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
  wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk && \
  apk add glibc-2.29-r0.apk && \
  rm glibc-2.29-r0.apk && \
  apk add --no-cache zlib libgcc && \
  DOCKER_COMPOSE_URL=https://github.com$(wget -q -O- https://github.com/docker/compose/releases/latest \
  | grep -Eo 'href="[^"]+docker-compose-Linux-x86_64' \
  | sed 's/^href="//' \
  | head -n1) && \
  wget -q -O /usr/local/bin/docker-compose $DOCKER_COMPOSE_URL && \
  chmod a+rx /usr/local/bin/docker-compose && \
  apk del --purge .deps && \
  docker-compose version

# K8S

RUN \
  curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.19.0/bin/linux/amd64/kubectl && \
  chmod +x ./kubectl && \
  mv ./kubectl /bin/kubectl

RUN \
  curl -sL https://get.helm.sh/helm-v3.2.3-linux-amd64.tar.gz | tar xzvf - && \
  chmod +x linux-amd64/helm && \
  mv linux-amd64/helm /bin/ && \
  rm -rf linux-amd64

RUN \
  curl -sL https://github.com/garethr/kubeval/releases/download/0.15.0/kubeval-linux-amd64.tar.gz | tar xzvf - -C /bin/

# Custom

RUN \
  wget -O gitlab-wait-for "https://gitlab.com/egeneralov/gitlab-wait-for/-/jobs/artifacts/1.0.0/raw/gitlab-wait-for-linux-amd64?job=linux-amd64" && \
  chmod +x gitlab-wait-for && \
  mv gitlab-wait-for /bin/

RUN \
  URL=$(curl -s https://api.github.com/repos/egeneralov/get-latest-registry-image-tag/releases/latest | jq -r .assets[].browser_download_url | grep linux) && \
  wget ${URL} -O /bin/get-latest-registry-image-tag && \
  chmod +x /bin/get-latest-registry-image-tag

